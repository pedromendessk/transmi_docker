# Docker configurations to run transmission for torrenting w/ VPN

A set of docker-compose configurations to download torrents.


## transmi_docker

> If you have not yet met the dependencies, please have a look at them bellow first.

Currently there are 2 container configurations available, with different configurations:
 * transmi_safe  - contains transmission behind a VPN
 * transmi_raw   - contains transmission 

To use any of the configurations 'cd' to the pretended directory.
These dockers use pre-built containers that can be customized. 



### Running the containers

To start the container just run the command:
```
docker-compose up
```

Open the web interface for transmission at http://localhost:9091/transmission/web


#### Configuring the VPN
> When using transmi_safe..

Place your .ovpn file in the directory that maps to /vpn for the openvpn container.

#### Adding torrents
Copy the .torrent files to the directory that maps to /watch for the transmission container.


### Helpful commands

#### Running the container interactively to debug
```
docker exec -it <container_name> bash
```



## Dependencies

> The following assume an Ubuntu OS >= 18.04

### 1. Install Docker Engine from [here](https://docs.docker.com/install/linux/docker-ce/ubuntu/).

> Bellow the resumed steps from the url above.
```
sudo apt-get update
sudo apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
```

To verify that the Docker Engine is installed:
```
sudo docker run hello-world
```

### 2. Install Docker Compose from [here](https://docs.docker.com/compose/install/).

> Bellow the resumed steps from the url above.
```
sudo curl -L "https://github.com/docker/compose/releases/download/1.25.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

To verify that Docker Compose is installed:
```
docker-compose --version
```

